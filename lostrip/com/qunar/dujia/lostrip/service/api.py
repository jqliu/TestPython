# !/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals

from flask import Flask
from werkzeug.contrib.fixers import ProxyFix

__author__ = "jianqiang.liu"

app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello world!"


@app.route("/test")
def test():
    return "Test"


app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == '__main__':
    app.run()

