# Overview

* test Python Features
* test Git-Flow

# Python Features

* xrange vs range
    * [Should you always favor xrange() over range()?](http://stackoverflow.com/questions/135041/should-you-always-favor-xrange-over-range "Should you always favor xrange() over range()?")

# Git-Flow

* test [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/ "Git flow")
    * [Why aren't you using git-flow](http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/ "Why aren't you using git-flow")
    * [Git Flow Intro](http://yakiloo.com/getting-started-git-flow/ "Git flow Intro")
    * [Git Flow Cheat Sheet](https://danielkummer.github.io/git-flow-cheatsheet/index.zh_CN.html "Git Flow Cheat Sheet")
    * [Customize Git-Flow](http://j.shirley.im/tech/git-flow/ "Customize My Own Git-Flow")

* more git flow info
    * how to track feature - 如果别处publish，自己要先track。修改之后commit之后再push. 别处要pull, commit, push.
    * how to track release - 和feature一样 先有commit，publish；track，pull，commit，push
    * how to track hot-fix - 和feature一样 

