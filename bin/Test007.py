#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals

from ExpHandlers import return_given_value_when_exception


@return_given_value_when_exception('Default Value when exception',
                                   (ImportError, ValueError))
def test_function(return_ok, exp_type='value'):
    if return_ok:
        return 5
    if exp_type == 'value':
        raise ValueError('invalid value')
    if exp_type == "import":
        raise ImportError('invalid import')
    raise AttributeError('invalid attribute')


if __name__ == "__main__":
    for exp_value in ('value', 'other', 'import'):
        # noinspection PyBroadException
        try:
            result = test_function(False, exp_value)
            print("OK - %s" % unicode(result))
        except Exception as e:
            print(" -- Exception %s" % unicode(e))
