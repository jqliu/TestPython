#@/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals

import os
from contextlib import contextmanager


@contextmanager
def ignore_error(*exceptions):
    try:
        yield
    except exceptions:
        pass


__author__ = 'jianqiang.liu'


if __name__ == "__main__":
    with ignore_error(Exception):
        os.remove("some_file.tmp")
        print("Removed")
