#!/usr/bin/env python
# coding: utf-8

import click

__author__ = 'jianqiang.liu'


@click.command()
@click.version_option("1.0", '-V', '--version', help="version")
@click.help_option('-?', '-h')
@click.option('-a', '--action', type=click.Choice(['Hello', 'Hi']),
              default='Hello', help='action to do', required=True)
@click.option('-v', '--verbose', metavar="verb", is_flag=True, flag_value=True, default=False)
@click.option("-c", "--count", default=1, help="number for the greetings")
@click.option('-f', '--filename', prompt="File Name",
              help='Data File to be process', required=True)
@click.option("-n", "--name", prompt="Your Name", help="Person to greet",
              required=True)
def hello(action, verbose, count, name, filename):
    if verbose:
        print(" Count: %d" % count)
        print('Action: %s, Name: %s!' % (action, name))

    for x in range(count):
        print('%s, %s!' % (action, name))
        print('Data File: %s' % filename)


if __name__ == "__main__":
    hello()
