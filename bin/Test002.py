#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals

__author__ = 'jianqiang.liu'


def find(seq, target_list):
    for i, value in enumerate(target_list):
        if value == seq:
            break
    else:
        return -1
    return i


if __name__ == "__main__":
    test_list = [1, 2, 3, 4]
    print(find(0, test_list))
    print(find(3, test_list))
