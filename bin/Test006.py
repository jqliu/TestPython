#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals

import signal
import functools
import time


class TimeoutError(Exception):
    pass


def timeout(seconds, error_message='Function call timed out'):
    def decorated(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return functools.wraps(func)(wrapper)

    return decorated


@timeout(seconds=1, error_message='Function slow; aborted')
def slow_function():
    time.sleep(5)


if __name__ == "__main__":
    try:
        slow_function()
        print("OK")
    except TimeoutError:
        print(" -- Timeout!!")
