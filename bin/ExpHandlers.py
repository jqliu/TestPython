#!/usr/bin/env python
# coding: utf-8
from __future__ import unicode_literals

import functools


def exp_handler(*p_args):
    """ An exception handling idiom using decorators"""

    def wrapper(f):
        if p_args:
            (handler, li) = p_args
            t = [(ex, handler) for ex in li]
            t.reverse()
        else:
            t = [(Exception, None)]

        def new_func(_t, *args, **kwargs):
            _ex, _handler = _t[0]

            try:
                if len(_t) == 1:
                    f(*args, **kwargs)
                else:
                    new_func(_t[1:], *args, **kwargs)
            except _ex, e:
                if _handler:
                    _handler(e)
                else:
                    print e.__class__.__name__, ':', e

        return functools.partial(new_func, t)

    return wrapper


def my_handler(e):
    print 'Caught exception!', e


def return_given_value_when_exception(exception_return, *exceptions):
    """
    如果执行的函数遇到异常，就返回指定的返回值
    :param exception_return: 遇到异常时的返回值
    :param exceptions: 捕获的异常清单
    :return: decorator
    """
    def decorated(func):
        if exceptions:
            t = [ex for ex in exceptions ]
            t.reverse()
        else:
            t = [Exception]

        def wrapper(_t, *args, **kwargs):
            _ex = _t[0]
            try:
                if len(_t) == 1:
                    result = func(*args, **kwargs)
                else:
                    result = wrapper(_t[1:], *args, **kwargs)
            except _ex, e:
                print e.__class__.__name__, ':', e
                result = exception_return
            return result

        return functools.partial(wrapper, t)

    return decorated
