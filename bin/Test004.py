#!/usr/bin/env python
# coding: utf-8
from __future__ import unicode_literals

from ExpHandlers import exp_handler, my_handler


# Examples
# Specify exceptions in order, first one is handled first
# last one last.

@exp_handler(my_handler, (ZeroDivisionError,))
@exp_handler(None, (AttributeError, ValueError))
def f1():
    return 1 / 0


@exp_handler()
def f3(*p_args):
    l = p_args
    return l.index(10)


if __name__ == "__main__":
    f1()
    f3()
